#ifndef _cDataBase_HG_
#define _cDataBase_HG_
#include <string>

/* Standard C++ includes */
#include <stdlib.h>
#include <iostream>

/*
Include directly the different
headers from cppconn/ and mysql_driver.h + mysql_util.h
(and mysql_connection.h). This will reduce your build time!
*/
#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include "sha256.h"
#include "Salt.h"
using namespace std;

class cDataBase
{
public:


	long long insertUser(std::string userName, std::string hashedPassword, std::string salt);

	
	long long selectUser(std::string userName, std::string &hashedPassword, std::string &salt, std::string &creationDate);


	long long createUserAccount(std::string userName, std::string password);


	long long authenticateAccount(std::string userName, std::string password, std::string &creationDate);


};

#endif // !_cDataBase_HG_

