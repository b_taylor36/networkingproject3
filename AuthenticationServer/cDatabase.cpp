#include "cDataBase.h"
#include <vector>

struct UserInfo
{
	long long authId = -1;
	long long userId = -1;
	std::string userName;
	std::string hashedPassword;
	std::string salt;
	std::string lastLogin;
	std::string creationDate;
};

std::vector<UserInfo> user;
long long gAuthId = 0;
long long  gUserId = 0;

bool findUserName(std::string userName)
{
	string resultEmail, queryAsString;

	try {
		sql::Driver *driver;
		sql::Connection *con;
		sql::Statement *stmt;
		sql::ResultSet  *res;

		/* Create a connection */
		driver = get_driver_instance();
		con = driver->connect("tcp://localhost:3306", "root", "root");

		/* Connect to the MySQL database */
		con->setSchema("info-6016");

		stmt = con->createStatement();

		queryAsString = "SELECT username FROM accounts WHERE username = '" + userName + "'";
		res = stmt->executeQuery(queryAsString.c_str());

		while (res->next())
		{
			resultEmail = res->getString("username");
		}

		delete res;
		delete stmt;
		delete con;
	}

	catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;

		return false;
	}

	if (resultEmail == userName)
	{
		return true;
	}

	return false;
}

long long cDataBase::insertUser(std::string username, std::string hashedPassword, std::string salt)
{

	if (findUserName(username))
	{
		// The user already exists
		return -1;
	}

	try {
		sql::Driver *driver;
		sql::Connection *con;
		sql::PreparedStatement  *prep_stmt;

		/* Create a connection */
		driver = get_driver_instance();
		con = driver->connect("tcp://localhost:3306", "root", "root");

		/* Connect to the MySQL database */
		con->setSchema("info-6016");

		prep_stmt = con->prepareStatement("INSERT INTO accounts( username, salt, hashed_password, userId ) VALUES( ? , ? , ? , ? )");
		prep_stmt->setString(1, username);	//email, 
		prep_stmt->setString(2, salt);	//salt
		prep_stmt->setString(3, hashedPassword); //hashed_password
		prep_stmt->setInt(4, ++gUserId);		//userId
		prep_stmt->execute();

		prep_stmt = con->prepareStatement("INSERT INTO user( last_login, creation_date )  VALUES( CURRENT_TIMESTAMP , CURRENT_TIMESTAMP )");
		prep_stmt->execute();

		delete prep_stmt;
		delete con;
	}

	catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}

	UserInfo thisUser;
	thisUser.authId = ++gAuthId;
	thisUser.userId = ++gUserId;
	thisUser.userName = username;
	thisUser.hashedPassword = hashedPassword;
	thisUser.salt = salt;
	thisUser.creationDate = "Some date";
	thisUser.lastLogin = "Some date";
	user.push_back(thisUser);
	return thisUser.userId;

}

long long cDataBase::selectUser(std::string username, std::string &hashedPassword, std::string &salt, std::string &creationDate)
{
	try {
		sql::Driver *driver;
		sql::Connection *con;
		sql::Statement *stmt;
		sql::ResultSet  *res;

		string queryAsString, userIdAsString;
		UserInfo thisUser;

		/* Create a connection */
		driver = get_driver_instance();
		con = driver->connect("tcp://localhost:3306", "root", "root");

		/* Connect to the MySQL database */
		con->setSchema("info-6016");

		stmt = con->createStatement();

		queryAsString = "SELECT id, username, salt, hashed_password, userId FROM accounts WHERE username = '" + username + "'";
		res = stmt->executeQuery(queryAsString.c_str());
		while (res->next())
		{
			thisUser.authId = res->getInt("id");
			thisUser.userId = res->getInt("userId");
			thisUser.userName = res->getString("username");
			thisUser.hashedPassword = res->getString("hashed_password");
			thisUser.salt = res->getString("salt");
		}

		if (thisUser.userName == username)
		{
			userIdAsString = std::to_string(thisUser.userId);
			queryAsString = "SELECT last_login, creation_date FROM user WHERE id = '" + userIdAsString + "'";
			res = stmt->executeQuery(queryAsString.c_str());
			while (res->next())
			{
				thisUser.creationDate = res->getString("creation_date");
				thisUser.lastLogin = res->getString("last_login");
			}

			// User exists update the last_login information
			sql::PreparedStatement  *prep_stmt;
			prep_stmt = con->prepareStatement("UPDATE user SET last_login = CURRENT_TIMESTAMP WHERE id = '" + userIdAsString + "'");
			prep_stmt->execute();

			delete prep_stmt;

			salt = thisUser.salt;
			creationDate = thisUser.creationDate;
			hashedPassword = thisUser.hashedPassword;

			delete stmt;
			delete res;
			delete con;

			return thisUser.userId;
		}

		// Authentication failed
		delete stmt;
		delete res;
		delete con;
	}

	catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;

		return -1;
	}

	return -1;
}


long long cDataBase::createUserAccount(std::string userName,
	std::string password)
{

	std::string salt;
	generateSalt(salt);
	std::string hashedPassord = sha256(password + salt);

	return insertUser(userName, hashedPassord, salt);

}

long long cDataBase::authenticateAccount(std::string userName,
	std::string password,
	std::string &creationDate)
{

	std::string dbHashedPassord;
	std::string dbSalt;
	long long result = selectUser(userName, dbHashedPassord,
		dbSalt, creationDate);

	if (result > 0) {
		// We get a user
		std::string userHashedPassword = sha256(password + dbSalt);
		if (userHashedPassword == dbHashedPassord) return result;
		else return -4;
	}

	return result;

}