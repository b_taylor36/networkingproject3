#define UNICODE
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <vector>
#include <cBuffer.h>
#include <authentication.pb.h>

#pragma comment(lib, "Ws2_32.lib")

enum messageIDs {
	CREATE_LOBBY = 1,
	LIST_LOBBIES,
	JOIN_LOBBY,
	LEAVE_LOBBY,
	LEAVE_ALL_LOBBIES,
	SEND_TEXT,
	CREATE_ACCOUNT,
	AUTHENTICATE,
	VALIDATE_SERVER,
	CREATE_ACCOUNT_WEB,
	CREATE_ACCOUNT_WEB_SUCCESS,
	CREATE_ACCOUNT_WEB_FAILURE,
	AUTHENTICATE_WEB,
	AUTHENTICATE_WEB_SUCCESS,
	AUTHENTICATE_WEB_FAILURE
};

const std::string DEFAULT_PORT = "5000";
const unsigned int DEFAULT_BUFFER_LENGTH = 512;


typedef struct SOCKET_INFORMATION {
	SOCKET_INFORMATION()
		: bIsAuthenticationServer(false)
		, bIsAuthenticated(false)
		, bHasData(false)
		, reqID(false)
		, userID(false)
	{

	}

	char charBuffer[DEFAULT_BUFFER_LENGTH];
	WSABUF buffer;
	SOCKET socket;
	DWORD numBytesSent;
	DWORD numBytesReturned;
	std::vector<std::string> lobbies;
	std::string userName;
	bool bIsAuthenticationServer;

	bool bIsAuthenticated;

	bool bHasData;

	long long reqID;

	long long userID;

} SOCKET_INFO, *LPSOCKET;


//Globals
LPSOCKET socketArray[FD_SETSIZE];


DWORD numSockets = 0;
int result;
FD_SET writeSet;
FD_SET readSet;
long long reqID = 0;


//Methods
BOOL createNewSocketInfo(SOCKET);
void releaseSocketInfo(DWORD Index);
void readSocketInfo(LPSOCKET);
void sendMessage(LPSOCKET, std::string msg, std::string userName);
void sendMessage(LPSOCKET, std::string msg);
void treatMessage(LPSOCKET, std::string msg);
void messageRoom(LPSOCKET, std::string room, std::string msg, std::string sender);



int main()
{

	SOCKET listenSocket;
	SOCKET acceptedSocket;
	WSADATA wsaData;
	DWORD socketsIndex;
	DWORD totalNumSockets;
	ULONG socketMode;
	DWORD Flags;
	DWORD sentBytes;
	DWORD receivedBytes;

	struct addrinfo* addrInfoResult;
	struct addrinfo hints;

	addrInfoResult = nullptr;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != 0) {
		printf("WSAStartup failed: %d\n", result);
		return 1;
	}


	listenSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (listenSocket == INVALID_SOCKET) {
		printf("socket() failed with error %d\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}


	getaddrinfo(NULL, "5000", &hints, &addrInfoResult);
	result = bind(listenSocket,
		addrInfoResult->ai_addr,
		(int)addrInfoResult->ai_addrlen);

	if (result == SOCKET_ERROR) {
		printf("bind() failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(addrInfoResult);
		closesocket(listenSocket);
		WSACleanup();
		return 1;
	}

	if (listen(listenSocket, SOMAXCONN)) {
		printf("listen() failed with error: %d\n", WSAGetLastError());
		closesocket(listenSocket);
		WSACleanup();
		return 1;
	}


	socketMode = 1;
	if (ioctlsocket(listenSocket, FIONBIO, &socketMode) == SOCKET_ERROR) {
		printf("ioctlsocket() failed with error %d\n", WSAGetLastError());
		return 1;
	}
	else
		printf("ioctlsocket() is OK!\n");


	while (true) {


		FD_ZERO(&readSet);
		FD_ZERO(&writeSet);

		FD_SET(listenSocket, &readSet);


		for (socketsIndex = 0; socketsIndex < numSockets; socketsIndex++)
			if (socketArray[socketsIndex]->bHasData)
				FD_SET(socketArray[socketsIndex]->socket, &writeSet);
			else
				FD_SET(socketArray[socketsIndex]->socket, &readSet);

		if ((totalNumSockets = select(0, &readSet, &writeSet, 0, 0))
			== SOCKET_ERROR) {
			printf("select() returned with error %d\n", WSAGetLastError());
			return 1;
		}
		else
			printf("select() is OK!\n");


		if (FD_ISSET(listenSocket, &readSet)) {
			totalNumSockets--;

			if ((acceptedSocket = accept(listenSocket, 0, 0))
				!= INVALID_SOCKET) {


				socketMode = 1;
				if (ioctlsocket(acceptedSocket, FIONBIO, &socketMode)
					== SOCKET_ERROR) {
					printf("ioctlsocket(FIONBIO) failed with error %d\n",
						WSAGetLastError());
					return 1;
				}
				else
					printf("ioctlsocket(FIONBIO) is OK!\n");

				if (createNewSocketInfo(acceptedSocket) == FALSE) {
					printf("createNewSocketInfo(acceptedSocket)"
						" failed!\n");
					return 1;
				}
				else
					printf("createNewSocketInfo() is OK!\n");

			}
			else {
				if (WSAGetLastError() != WSAEWOULDBLOCK) {
					printf("accept() failed with error %d\n",
						WSAGetLastError());
					return 1;
				}
				else
					printf("accept() is fine!\n");
			}

		}
		for (socketsIndex = 0; totalNumSockets > 0
			&& socketsIndex < numSockets;
			socketsIndex++) {

			LPSOCKET SocketInfo = socketArray[socketsIndex];


			if (FD_ISSET(SocketInfo->socket, &readSet)) {
				totalNumSockets--;

				SocketInfo->buffer.buf = SocketInfo->charBuffer;
				SocketInfo->buffer.len = DEFAULT_BUFFER_LENGTH;

				Flags = 0;
				if (WSARecv(SocketInfo->socket,
					&(SocketInfo->buffer), 1,
					&receivedBytes, &Flags, NULL, NULL)
					== SOCKET_ERROR) {

					if (WSAGetLastError() != WSAEWOULDBLOCK) {
						printf("WSARecv() failed with error %d\n",
							WSAGetLastError());
						releaseSocketInfo(socketsIndex);
					}
					else
						printf("WSARecv() is OK!\n");

					continue;

				}
				else {

					SocketInfo->numBytesReturned = receivedBytes;


					if (receivedBytes == 0) {
						releaseSocketInfo(socketsIndex);
						continue;
					}
					else
						readSocketInfo(socketArray[socketsIndex]);

				}
			}



			if (FD_ISSET(SocketInfo->socket, &writeSet)) {
				totalNumSockets--;

				if (WSASend(SocketInfo->socket,
					&(SocketInfo->buffer),
					1, &sentBytes, 0, NULL, NULL)
					== SOCKET_ERROR) {

					if (WSAGetLastError() != WSAEWOULDBLOCK) {
						printf("WSASend() failed with error %d\n",
							WSAGetLastError());
						releaseSocketInfo(socketsIndex);
					}
					else
						printf("WSASend() is OK!\n");

					continue;

				}
				else {

					SocketInfo->numBytesSent += sentBytes;

					if (SocketInfo->numBytesSent == SocketInfo->numBytesReturned) {
						SocketInfo->numBytesSent = false;
						SocketInfo->numBytesReturned = false;
					}

				}


				SocketInfo->bHasData = false;

			}
		}
	}
}


BOOL createNewSocketInfo(SOCKET s)
{
	LPSOCKET SI;

	printf("Accepted socket number %d\n", s);

	if ((SI = (LPSOCKET)GlobalAlloc(GPTR, sizeof(SOCKET_INFO)))
		== NULL) {
		printf("GlobalAlloc() failed with error %d\n", GetLastError());
		return FALSE;
	}
	else
		printf("GlobalAlloc() for SOCKET_INFO is OK!\n");


	SI->socket = s;
	SI->numBytesSent = 0;
	SI->numBytesReturned = 0;

	socketArray[numSockets] = SI;
	numSockets++;
	return(TRUE);
}


void releaseSocketInfo(DWORD Index)
{
	LPSOCKET SI = socketArray[Index];
	DWORD i;

	closesocket(SI->socket);
	printf("Closing socket number %d\n", SI->socket);
	GlobalFree(SI);


	for (i = Index; i < numSockets; i++) {
		socketArray[i] = socketArray[i + 1];
	}

	numSockets--;
}


void readSocketInfo(LPSOCKET sa)
{
	bool isMsgIncomplete = true;
	int currMsgLength = 0;
	std::string currBuffer;


	for (int i = 0; i <= sa->numBytesReturned; i++) {


		if (isMsgIncomplete) {


			if (!currMsgLength) {


				currBuffer.push_back(sa->buffer.buf[i]);


				if (currBuffer.size() == 4) {


					cBuffer buff(4);
					for (int j = 0; j < 4; j++) {
						buff.serializeChar(currBuffer.at(j));
					}
					currMsgLength = buff.deserializeIntBE();

				}
			}
			else {


				if (currMsgLength != currBuffer.size()) {
					currBuffer.push_back(sa->buffer.buf[i]);

				}
				else {


					treatMessage(sa, currBuffer);
					currBuffer = "";
					!isMsgIncomplete;
					!currMsgLength;

				}

			}

		}
		else {


			currMsgLength = 0;
			!isMsgIncomplete;
			currBuffer.push_back(sa->buffer.buf[i]);

		}
	}
}


void treatMessage(LPSOCKET sa, std::string msg)
{
	cBuffer buff(msg.size());


	for (int i = 0; i < msg.size(); i++)
		buff.serializeChar(msg.at(i));

	int packetLength = buff.deserializeIntBE();


	if (packetLength > 65542)
		return;

	char msgID = buff.deserializeChar();


	switch (msgID) {

	case messageIDs::JOIN_LOBBY: {


		if (!sa->bIsAuthenticated) {
			sendMessage(sa, "You have to authenticate before joing a Lobby!",
				"Chat Server");
		}
		else {

			std::string roomName;
			char roomNameLength = buff.deserializeChar();
			for (short i = 0; i < roomNameLength; i++)
				roomName.push_back(buff.deserializeChar());

			sa->lobbies.push_back(roomName);

			std::string userName;
			char userNameLength = buff.deserializeChar();

			for (short i = 0; i < userNameLength; i++)
				userName.push_back(buff.deserializeChar());


			sa->userName = userName;
			sa->userName = userName;


			messageRoom(sa, roomName, " has connected to ", "Chat Server");

		}

	}
					 break;

	case messageIDs::LEAVE_LOBBY: {

		std::string roomName;
		short roomNameLength = buff.deserializeChar();
		for (short i = 0; i < roomNameLength; i++)
			roomName.push_back(buff.deserializeChar());


		for (int i = 0; i < sa->lobbies.size(); i++)
			if (roomName == sa->lobbies[i])
				sa->lobbies[i].erase();


		messageRoom(sa, roomName, " has disconnected from ", "Chat Server");

	}
					  break;

	case messageIDs::SEND_TEXT: {

		std::string msg;
		short msgLength = buff.deserializeShortBE();
		for (short i = 0; i < msgLength; i++)
			msg.push_back(buff.deserializeChar());


		for (int i = 0; i < sa->lobbies.size(); i++)
			for (int j = 0; j < numSockets; j++)
				for (int k = 0; k < socketArray[j]->lobbies.size(); k++)
					if (sa->lobbies.at(i) == socketArray[j]->lobbies.at(k)
						&& sa->lobbies.at(i) != "")
						sendMessage(socketArray[j], msg, sa->userName);

	}
					break;

	case messageIDs::CREATE_ACCOUNT: {

		std::string email;
		char emailLength = buff.deserializeChar();
		for (short i = 0; i < emailLength; i++)
			email.push_back(buff.deserializeChar());

		std::string password;
		char passwordLength = buff.deserializeChar();
		for (short i = 0; i < passwordLength; i++)
			password.push_back(buff.deserializeChar());


		authentication::CreateAccountWeb caw;
		reqID++;
		sa->reqID = reqID;
		caw.set_requestid(reqID);
		caw.set_email(email);
		caw.set_plaintextpassword(password);

		std::string pbStr;
		pbStr = caw.SerializeAsString();


		int messageLength = 0;


		messageLength = sizeof(char) + pbStr.size();
		cBuffer authBuff(messageLength);
		authBuff.serializeChar(messageIDs::CREATE_ACCOUNT_WEB);
		for (int i = 0; i < pbStr.size(); i++)
			authBuff.serializeChar(pbStr.at(i));


		for (int i = 0; i < numSockets; i++)
			if (socketArray[i]->bIsAuthenticationServer)
				sendMessage(socketArray[i], authBuff.getBuffer());

	}
						 break;

	case messageIDs::AUTHENTICATE: {

		std::string email;
		char emailLength = buff.deserializeChar();
		for (short i = 0; i < emailLength; i++)
			email.push_back(buff.deserializeChar());

		std::string password;
		char passwordLength = buff.deserializeChar();
		for (short i = 0; i < passwordLength; i++)
			password.push_back(buff.deserializeChar());


		authentication::AuthenticateWeb aw;
		reqID++;
		sa->reqID = reqID;
		aw.set_requestid(reqID);
		aw.set_email(email);
		aw.set_plaintextpassword(password);

		std::string pbStr;
		pbStr = aw.SerializeAsString();


		int messageLength = 0;

		messageLength = sizeof(char) + pbStr.size();
		cBuffer authBuff(messageLength);
		authBuff.serializeChar(messageIDs::AUTHENTICATE_WEB);
		for (int i = 0; i < pbStr.size(); i++)
			authBuff.serializeChar(pbStr.at(i));

		for (int i = 0; i < numSockets; i++)
			if (socketArray[i]->bIsAuthenticationServer)
				sendMessage(socketArray[i], authBuff.getBuffer());

	}
					   break;

	case messageIDs::VALIDATE_SERVER: {

		std::string serverName;
		char serverNameLength = buff.deserializeChar();
		for (int i = 0; i < serverNameLength; i++)
			serverName.push_back(buff.deserializeChar());

		std::string hash;
		char hashLength = buff.deserializeChar();
		for (int i = 0; i < hashLength; i++)
			hash.push_back(buff.deserializeChar());


		if (hash != "TEMP_HASH") {
			std::cout << "Authentication Error!\n";
			return;
		}


		sa->bIsAuthenticationServer = true;


		sa->userName = serverName;
		sa->userName = serverName;

		std::cout << "Authentication Server validated!\n";
		sendMessage(sa, "Authentication Server validated!\n", "Chat Server");

	}
						  break;

	case messageIDs::CREATE_ACCOUNT_WEB_SUCCESS: {


		if (sa->bIsAuthenticationServer) {

			std::string receivedStr;
			short msgLength = buff.deserializeShortBE();
			for (int i = 0; i < msgLength; i++)
				receivedStr.push_back(buff.deserializeChar());


			authentication::CreateAccountWebSuccess caws;
			caws.ParseFromString(receivedStr);


			for (int i = 0; i < numSockets; i++) {
				if (socketArray[i]->reqID == caws.requestid()) {
					socketArray[i]->userID = caws.userid();
					sendMessage(socketArray[i],
						"The user was created successfully!",
						"Authentication Server");
				}
			}
		}
	}
									 break;

	case messageIDs::CREATE_ACCOUNT_WEB_FAILURE: {


		if (sa->bIsAuthenticationServer) {
			std::string receivedStr;
			short msgLength = buff.deserializeShortBE();
			for (int i = 0; i < msgLength; i++)
				receivedStr.push_back(buff.deserializeChar());


			authentication::CreateAccountWebFailure cawf;
			cawf.ParseFromString(receivedStr);


			for (int i = 0; i < numSockets; i++) {
				if (socketArray[i]->reqID == cawf.requestid()) {

					char reason = cawf.thereaseon();

					switch (reason) {
						case authentication
							::CreateAccountWebFailure_reason_ACCOUNT_ALREADY_EXISTS:
								sendMessage(socketArray[i],
									"The account already exists!",
									"Authentication Server");
								break;

								case authentication
									::CreateAccountWebFailure_reason_INVALID_PASSWORD:
										sendMessage(socketArray[i],
											"The Authentication Server did "
											"not accept your password!",
											"Authentication Server");
										break;

										case authentication
											::CreateAccountWebFailure_reason_INTERNAL_SERVER_ERROR:
												sendMessage(socketArray[i],
													"There was an Internal Authentication "
													"Server error!",
													"Authentication Server");
												break;


										default:
											sendMessage(socketArray[i],
												"There was an unknown error!",
												"Authentication Server");

					}
				}
			}
		}
	}
									 break;

	case messageIDs::AUTHENTICATE_WEB_SUCCESS: {


		if (sa->bIsAuthenticationServer) {

			std::string receivedStr;
			short msgLength = buff.deserializeShortBE();
			for (int i = 0; i < msgLength; i++)
				receivedStr.push_back(buff.deserializeChar());


			authentication::AuthenticateWebSuccess aws;
			aws.ParseFromString(receivedStr);


			for (int i = 0; i < numSockets; i++) {
				if (socketArray[i]->reqID == aws.requestid()) {
					socketArray[i]->bIsAuthenticated = true;
					sendMessage(socketArray[i],
						"Authentication successful, account created on "
						+ aws.creationdate(),
						"Authentication Server");
				}
			}
		}
	}
								   break;

	case messageIDs::AUTHENTICATE_WEB_FAILURE: {


		if (sa->bIsAuthenticationServer) {

			std::string receivedStr;
			short msgLength = buff.deserializeShortBE();
			for (int i = 0; i < msgLength; i++)
				receivedStr.push_back(buff.deserializeChar());


			authentication::AuthenticateWebFailure awf;
			awf.ParseFromString(receivedStr);


			for (int i = 0; i < numSockets; i++) {
				if (socketArray[i]->reqID == awf.requestid()) {

					char reason = awf.thereaseon();

					switch (reason) {
						case authentication
							::AuthenticateWebFailure_reason_INVALID_CREDENTIALS:
								sendMessage(socketArray[i],
									"Invalid credentials!",
									"Authentication Server");
								break;

								case authentication
									::AuthenticateWebFailure_reason_INTERNAL_SERVER_ERROR:
										sendMessage(socketArray[i],
											"There was an internal error",
											"Authentication Server");
										break;

								default:
									sendMessage(socketArray[i],
										"There was an unknown error!",
										"Authentication Server");

					}
				}
			}
		}
	}
								   break;

	default:
		break;

	}
}


void sendMessage(LPSOCKET sa, std::string msg, std::string userName)
{

	std::string formatedMsg = userName + "->" + msg;


	if (formatedMsg.size() > 65535)
		return;


	int packetLength = sizeof(INT32) + formatedMsg.size();


	cBuffer buff(packetLength);
	buff.serializeIntLE(packetLength);

	for (int i = 0; i < formatedMsg.size(); i++)
		buff.serializeChar(formatedMsg.at(i));

	buff.serializeChar('\0');

	sa->buffer.buf = buff.getBuffer();
	sa->buffer.len = packetLength;
	sa->bHasData = true;
}


void sendMessage(LPSOCKET sa, std::string msg)
{

	if (msg.size() > 65535)
		return;


	int packetLength = sizeof(INT32) + msg.size();


	cBuffer buff(packetLength);
	buff.serializeIntLE(packetLength);

	for (int i = 0; i < msg.size(); i++)
		buff.serializeChar(msg.at(i));



	sa->buffer.buf = buff.getBuffer();
	sa->buffer.len = packetLength;
	sa->bHasData = true;
}


void messageRoom(LPSOCKET sa, std::string room, std::string msg, std::string sender)
{
	for (int i = 0; i < numSockets; i++)
		for (int j = 0; j < socketArray[i]->lobbies.size(); j++)
			if (socketArray[i]->lobbies.at(j) == room)
				sendMessage(socketArray[i], sa->userName + msg + room, sender);
}