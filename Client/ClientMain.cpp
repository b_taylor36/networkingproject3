#include <iostream>
#include <string>
#include <vector>
#include "cConnection.h"

enum messageIDs {
	CREATE_LOBBY = 1,
	LIST_LOBBIES,
	JOIN_LOBBY,
	LEAVE_LOBBY,
	LEAVE_ALL_LOBBIES,
	SEND_TEXT,
	CREATE_ACCOUNT,
	AUTHENTICATE,
	VALIDATE_SERVER,
	CREATE_ACCOUNT_WEB,
	CREATE_ACCOUNT_WEB_SUCCESS,
	CREATE_ACCOUNT_WEB_FAILURE,
	AUTHENTICATE_WEB,
	AUTHENTICATE_WEB_SUCCESS,
	AUTHENTICATE_WEB_FAILURE
};



using namespace std;

#define PROMPT "->"

// Function signatures
void checkRoom(InitInfo&, string, vector<string>&);
int readCase(string&, InitInfo&, vector<string>&, cConnection&);

int main()
{

	InitInfo user;  // user struct data

					// Prepare a friendely welcome screen
	cout << "========================================";
	cout << "=======================================\n";
	cout << "                            Game Lobby!      \n";
	cout << "========================================";
	cout << "=======================================\n";
	cout << "Please type in the ip adress of the server\n"
		<< PROMPT;

	// Get the server address
	char serverAddr[MAX_ARRAY_SIZE];
	cin >> serverAddr;
	user.serverAddr = serverAddr;

	// Read the user's first name
	cout << "Please type in your first name:\n" << PROMPT;
	string firstName;
	cin >> firstName;
	user.firstName = firstName;

	// Try to make a connection
	cConnection myConn;
	myConn.connectToServer(user);

	if (myConn.m_isAlive) {
		system("cls");
		cout << "========================================";
		cout << "=======================================\n";
		cout << "                            Game Lobby!      \n";
		cout << "========================================";
		cout << "========================================\n";
		cout << user.firstName
			<< ", you are now connected to the server.\n"
			<< "You can type -help for a list of commands.\n\n";
		system("pause");
	}
	else {
		cout << "Something went wrong. We didn't connected to the server!\n";
		Sleep(6000);
	}

	// Loop variables
	string myMessage;               // The message that the this client typed
	string srvMessage;              // The message that comes from the server
	string chatBuffer;              // Stores all the received messages
	vector<string> connectedRooms;  // Stores the connected rooms locally


									// Main chat loop
	while (myConn.m_isAlive) {
		// Get new messages
		srvMessage = myConn.getMessages();

		// Is there a new message from Server?
		if (srvMessage != "") {
			checkRoom(user, srvMessage, connectedRooms);
			chatBuffer += srvMessage;
		}

		// Format the chat screen - The best way we could so far! :/
		system("cls");
		for (int i = 0; i < 30; i++)
			cout << '\n';

		cout << chatBuffer;
		cout << "________________________________________";
		cout << "________________________________________\n";
		cout << "Press Enter to update messages. Type -help for help.\n";
		cout << "Connected to ";
		for (int i = 0; i < connectedRooms.size(); i++)
			cout << connectedRooms[i] << " ";
		cout << '\n';
		cout << "========================================";
		cout << "========================================\n";

		// Get input
		cout << PROMPT;
		getline(cin, myMessage);

		// Treat the input
		if (readCase(myMessage, user, connectedRooms, myConn))
			break;  // Received an exit signal

	}//!while (myConn.isAlive)

	 // There is no connection anymore. Close it!
	myConn.closeConnection();
	system("cls");
	cout << "========================================";
	cout << "========================================\n";
	cout << "              Thanks for using Game Lobby. See you soon!\n";
	cout << "========================================";
	cout << "========================================\n";
	Sleep(4000);


	return 0;
}


//-----------------------------------------------------------------------------
// Helper Functions

/**
Check if we were connected to a room by the Chat Server. If yes, we
update our local rooms vector, so we can see the information on screen.
*/
void checkRoom(InitInfo& user, string message, vector<string>& connectedRooms)
{
	// The beggining og the message should be:
	string msgHead = "Chat Server->" + user.firstName + " has connected to ";

	// Is it big enough?
	if (message.size() > msgHead.size()) {
		// Check if the begginings match
		for (int i = 0; i < msgHead.size(); i++)
			if (message.at(i) != msgHead.at(i))
				return;

		// Format the string to contain only the room name
		string theRoom = message.substr(msgHead.size(),
			message.size() - msgHead.size() - 1);
		connectedRooms.push_back(theRoom);
	}
}

/**
Reads and treats all messages received from the server
*/
int readCase(string& myMessage,
	InitInfo& user,
	vector<string>& connectedRooms,
	cConnection& myConn)
{
	// HELP
	if (myMessage == "-help") {
		system("cls");

		for (int i = 0; i < 30; i++)
			cout << '\n';

		cout << "List of current commands:\n"
			<< "-register | Creates a new user\n"
			<< "-login    | Login into your account;\n"
			<< "-logout    | Logout of your account;\n"
			<< "-create   | Creates a Game Lobby;\n"
			<< "-list     | View a list of Game Lobbies;\n"
			<< "-join     | Joins a Game Lobby;\n"
			<< "-leave    | Leaves a Game Lobby;\n"
			<< "-exit     | Leaves the Game Lobby application.\n"
			<< "========================================"
			<< "========================================\n"
			<< PROMPT;

		// Get the new command
		getline(cin, myMessage);

	}

	// NEW user
	if (myMessage == "-register") {
		myMessage = "";
		cout << "Please type in your User Name\n";
		cout << PROMPT;
		string answer;
		cin >> answer;
		user.userName = answer;
		cout << "Please type in your password\n";
		cout << PROMPT;
		cin >> answer;
		while (answer.size() < 6) {
			cout << "Password should be at least 6 characters. Try again!\n";
			cout << PROMPT;
			cin >> answer;
		}
		user.password = answer;
		myConn.sendMessage(user, CREATE_ACCOUNT, "");
	}

	// AUTHENTICATION
	if (myMessage == "-login") {
		myMessage = "";
		cout << "Please type in your User Name\n";
		cout << PROMPT;
		string answer;
		cin >> answer;
		user.userName = answer;
		cout << "Please type in your password\n";
		cout << PROMPT;
		cin >> answer;
		while (answer.size() < 6) {
			cout << "Password should be at least 6 characters. Try again!\n";
			cout << PROMPT;
			cin >> answer;
		}
		user.password = answer;
		myConn.sendMessage(user, AUTHENTICATE, "");
	}

	// Create a Game Lobby
	if (myMessage == "-create")
	{
		myMessage = "";
		cout << "Please type in the name of the Game Lobby to create\n";
		cout << PROMPT;
		string answer;
		cin >> answer;
		user.lobbyInfo.lobbyName = answer;
		cout << "Please type in a map name for the Game Lobby to create\n";
		cout << PROMPT;
		cin >> answer;
		user.lobbyInfo.mapName = answer;
		cout << "Please type in max number of players to the Game Lobby\n";
		cout << PROMPT;
		unsigned char maxNum;
		cin >> maxNum;
		user.lobbyInfo.maxNumPlayers = maxNum;
		myConn.sendMessage(user, CREATE_LOBBY, "");
	}

	// List available Game Lobbies
	if (myMessage == "-list")
	{
		myMessage = "";
		myConn.sendMessage(user, LIST_LOBBIES, "");
	}

	// JOIN a Game Lobby
	if (myMessage == "-join") {
		myMessage = "";
		cout << "Please type in the name of the Game Lobby\n";
		cout << PROMPT;
		string answer;
		cin >> answer;
		user.lobbyInfo.lobbyName = answer;
		myConn.sendMessage(user, JOIN_LOBBY, "");
	}

	// LEAVE a room
	if (myMessage == "-leave") {
		myMessage = "";
		cout << "Please type in the name of the Game Lobby to leave from\n";
		cout << PROMPT;
		string answer;
		cin >> answer;
		user.lobbyInfo.lobbyName = answer;
		for (int i = 0; i < connectedRooms.size(); i++) {
			if (connectedRooms.at(i) == user.lobbyInfo.lobbyName)
				connectedRooms.at(i).erase();
		}
		myConn.sendMessage(user, LEAVE_LOBBY, "");
	}

	// EXIT
	if (myMessage == "-exit") {
		// Signal to exit
		myConn.sendMessage(user, LEAVE_ALL_LOBBIES, "");
		return 1;
	}

	// There is a message. Send it!
	if (myMessage != "") {
		myConn.sendMessage(user, SEND_TEXT, myMessage);
	}

	return 0;

}
